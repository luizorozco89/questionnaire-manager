import React from 'react';
import PropTypes from 'react-proptypes';
import styled from 'styled-components';
import Modal from '../../../common/components/modal/modal';
import Button from '../../../common/components/button';
import QuestionForm from '../question-form/question-form';

const StyledQuestionList = styled.div`
    header {
        margin-bottom: 10px;

        label {
            cursor: pointer;

            i {
                margin-right: 3px;
            }
        }

        span {
            float: right;
            color: #5bc0de;
            cursor: pointer;

            i {
                margin-right: 3px;
                font-size: 14px;
            }
        }
    }

    > section {
        div {
            background-color: #f5f5f5;
            padding: 11px 8px;
            border-radius: 1px;
            margin-bottom: 7px;
            border: 1px solid #f0f0f0;
        }

        span {
            float: right;
            background-color: #428bca;
            color: white;
            width: 68px;
            height: 20px;
            text-align: center;
            border-radius: 3px;
            font-size: 13px;
            padding-top: 2px;
        }

        &.hide {
            display: none;
        }
    }

    footer {
        background: #F0F0F0;
        margin: 20px -20px -20px -20px;
        padding: 10px 20px;

        .cancel-button {
            float: right;
        }

    }
`;

class QuestionList extends React.Component {

    static propTypes = {
        questions: PropTypes.array,
        className: PropTypes.string,
        onSaveQuestion: PropTypes.func,
        getOptions: PropTypes.func
    };

    state = {
        selectedQuestion: null,
        showHideSection: true,
        showModal: false
    };

    handleCloseModal = () => this.setState({ showModal: false, selectedQuestion: null });

    handleShowModal = () => this.setState({ showModal: true });

    handleShowHideSection = () => this.setState({showHideSection: !this.state.showHideSection});

    handleSelectQuestion = (e) => {
        const questionId = parseInt(e.target.getAttribute('data-id'));
        const selectedQuestion = this.props.questions
            .filter((question) => question.id === questionId)[0];
        this.setState({ selectedQuestion, showModal: true });
    };

    handleQuestionChange = selectedQuestion => this.setState({ selectedQuestion });

    isValidQuestion = (question) => {
        var isValid = question ? true : false;
        if((question && question.question.trim() === '') || 
           (question && question.type !== 'yes/no' && question.correctAnswer.trim() === '')) {
           isValid = false;
        }
        return isValid;
    };

    handleSaveQuestion = () => {
        this.props.onSaveQuestion(this.state.selectedQuestion);
        this.handleCloseModal();
    };

    render() {
        const { questions, className, onSaveNewQuestion } = this.props,
            clsName = this.state.showHideSection ?
                'fa fa-sort-up' : 'fa fa-caret-down';

        return (
            <StyledQuestionList className={className}>
                <header>
                    <label onClick={this.handleShowHideSection}>
                        <i className={clsName} />
                        Questions ({questions.length})
                    </label>
                    <span onClick={this.handleShowModal}>
                        <i className="fa fa-plus" />Add
                    </span>
                </header>
                <section className={this.state.showHideSection ? '' : 'hide'}>
                    {questions.map(question =>
                        <div
                            key={question.id}
                            data-id={question.id}
                            onClick={this.handleSelectQuestion}>
                            {question.question}<span>{question.type}</span>
                        </div>
                    )}
                </section>
                <Modal 
                    show={this.state.showModal}
                    title={ this.state.selectedQuestion && this.state.selectedQuestion.id ? 'Editing question' : 'Adding new question' }
                    onClose={this.handleCloseModal}>
                    <div>
                        <QuestionForm
                            question={this.state.selectedQuestion}
                            onChange={this.handleQuestionChange} />
                    </div>
                    <footer>
                        <Button
                            className="primary"
                            type="button"
                            disabled={!this.isValidQuestion(this.state.selectedQuestion)}
                            onClick={this.handleSaveQuestion}>Save</Button>
                        <Button
                            className="danger cancel-button"
                            type="button"
                            onClick={this.handleCloseModal}>Cancel</Button>
                    </footer>
                </Modal>
            </StyledQuestionList>  
        );
    }

}

export default QuestionList;