import React from "react";
import PropTypes from "react-proptypes";
import styled from "styled-components";
import moment from "moment";

const StyledQuestionaresListItem = styled.div`
  margin-bottom: 10px;
  padding: 10px;
  border: 1px solid #e0e0e0;
  border-radius: 3px;
  background-color: #fff;
  position: relative;

  #questions-date {
    position: absolute;
    top: 13px;
    left: 296px;
    right: 211px;
    color: #858c9c;

    #questions-number {
      margin-right: 66px;
    }
  }

  p {
    margin-top: 0;

    .name {
      margin-right: 20px;
      max-width: 45%;
      white-space: nowrap;
      overflow: hidden;
      text-overflow: ellipsis;
      display: inline-block;
    }

    > i {
      float: right;
      cursor: pointer;
    }
  }

  div {
    margin-top: 15px;

    .description {
      color: #858c9c;
      max-width: 85%;
      display: inline-block;
      font-size: 13px;
      font-weight: 500;
    }

    .status {
      float: right;
      text-transform: uppercase;
      font-size: 14px;
      position: absolute;
      bottom: 10px;
      right: 10px;

      &.created {
        color: #2a7c9b;
      }

      &.ready {
        color: #06d9f6;
      }

      &.done {
        color: #72bb53;
      }

      i {
        margin-right: 5px;
      }
    }
  }
`;

class QuestionaresListItem extends React.Component {
  static propTypes = {
    questionare: PropTypes.object,
    onSelect: PropTypes.func,
    onRemoveQuestionare: PropTypes.func
  };

  handleSelect = () => {
    this.props.onSelect(this.props.questionare);
  };

  handleRemoveQuestionare = e => {
    e.stopPropagation();
    this.props.onRemoveQuestionare(this.props.questionare);
  };

  render() {
    const { questionare } = this.props;

    return (
      <StyledQuestionaresListItem onClick={this.handleSelect}>
        <div id="questions-date">
          <label id="questions-number">
            {questionare.getQuestions().length} Questions
          </label>
          <label id="date">{moment(questionare.date).format("LL")}</label>
        </div>
        <p>
          <label className="name">{questionare.name}</label>
          <i className="fa fa-times" onClick={this.handleRemoveQuestionare} />
        </p>
        <div>
          <label className="description">{questionare.description}</label>
          <span className={`status ${questionare.status}`}>
            <i className="fa fa-clock-o" />
            {questionare.status}
          </span>
        </div>
      </StyledQuestionaresListItem>
    );
  }
}

export default QuestionaresListItem;
