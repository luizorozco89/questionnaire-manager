import React from "react";
import PropTypes from "react-proptypes";
import styled from "styled-components";
import StyledFormField from "../../../common/components/styled-form-field";
import TextField from "../../../common/components/textfield";
import TextArea from "../../../common/components/textarea";
import Select from "../../../common/components/select";
import QuestionList from "../question-list/question-list";

const StyledQuestionareForm = styled.div`
  .question-list {
    margin-top: 100px;
  }
`;

class QuestionareForm extends React.Component {
  static propTypes = {
    onChange: PropTypes.func,
    questionare: PropTypes.object,
    onSaveQuestion: PropTypes.func
  };

  constructor(props) {
    super(props);

    this.state = {
      questionare: this.initializeQuestionare(props)
    };
  }

  componentWillReceiveProps(nextProps) {
    this.setState({
      questionare: this.initializeQuestionare(nextProps)
    });
  }

  initializeQuestionare = ({ questionare }) => questionare;

  handleChange = e => {
    const questionare = {
      ...this.state.questionare,
      [e.target.name]: e.target.value
    };
    this.setState({ questionare });
    this.props.onChange(questionare);
  };

  render() {
    const { questionare, onSaveQuestion } = this.props;

    return (
      <StyledQuestionareForm>
        <StyledFormField>
          <label>Name</label>
          <TextField
            name="name"
            onChange={this.handleChange}
            value={questionare.name}
          />
        </StyledFormField>
        <StyledFormField>
          <label>Description</label>
          <TextArea
            name="description"
            className="description"
            onChange={this.handleChange}
            value={questionare.description}
          />
        </StyledFormField>
        <StyledFormField>
          <label>Status</label>
          <Select
            name="status"
            value={questionare.status}
            onChange={this.handleChange}
          >
            <option value="created">Created</option>
            <option value="ready">Ready</option>
            <option value="done">Done</option>
          </Select>
        </StyledFormField>

        <QuestionList
          className="question-list"
          questions={questionare.getQuestions()}
          onSaveQuestion={onSaveQuestion}
        />
      </StyledQuestionareForm>
    );
  }
}

export default QuestionareForm;
