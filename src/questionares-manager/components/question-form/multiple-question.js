import React from 'react';
import PropTypes from 'react-proptypes';
import styled from 'styled-components';
import StyledFormField from '../../../common/components/styled-form-field';
import TextField from '../../../common/components/textfield';
import Button from '../../../common/components/button';

const StyledMultipleQuestion = styled.div`
    .option {
        background-color: #f5f5f5;
        height: 26px;
        padding: 10px 10px 0px 10px;
        margin: 5px 0px;
        border-radius: 1px;

        .option-text {
            margin-left: 12px;
        }

        i {
            float: right;
        }
    }

    .add-options-container {
        display: flex;
        align-items: center;
        justify-content: space-between;

        .opt-key {
            width: 50px;
        }

        .opt-text {
            flex-grow: 2;
            margin: 0 10px;
        }
    }

`;

class MultipleQuestion extends React.Component {

    static propTypes = {
        value: PropTypes.string,
        options: PropTypes.object,
        onChange: PropTypes.func
    };

    state = {
        key: '',
        option: ''
    };

    handleOptionsChange = (e) => {
        this.setState({ [e.target.name]: e.target.value });
    };

    handleSetOptions = (options) => {
        const { onChange } = this.props;
        const e = {
            target: { name: 'options', value: options }
        };
        onChange(e);
    };

    handleAddOption = () => {
        const { options } = this.props;
        const _options = { ...options, [this.state.key]: this.state.option };
        this.handleSetOptions(_options);
        this.setState({key: '', option: ''});
    };

    handleRemoveOption = (e) => {
        const key = e.target.getAttribute('data-key');
        const { options } = this.props;
        const _options = { ...options };
        delete _options[key];
        this.handleSetOptions(_options);
    };

    render () {
        const { value, options, onChange } = this.props,
            isDisabled = this.state.key !== '' && this.state.option !== '' ? false : true;

        return (
            <StyledMultipleQuestion>
                <StyledFormField>
                    <label>Answer</label>
                    <TextField
                        name="correctAnswer"
                        type="text"
                        value={value}
                        onChange={onChange} />
                    <div>
                        <label>Options</label>
                        {Object.keys(options || {}).map(key =>
                            <div className="option" key={key}>
                                <span>{key}</span>
                                <span className="option-text">{options[key]}</span>
                                <i
                                    className="fa fa-close"
                                    data-key={key}
                                    onClick={this.handleRemoveOption} />
                            </div>)}
                        <div className="add-options-container">
                            <TextField
                                name="key"
                                className="opt-key"
                                placeholder="Key"
                                value={this.state.key}
                                onChange={this.handleOptionsChange} />
                            <TextField
                                name="option"
                                className="opt-text"
                                placeholder="Option"
                                value={this.state.option}
                                onChange={this.handleOptionsChange} />
                            <Button
                                className="primary"
                                name="options"
                                disabled={isDisabled}
                                onClick={this.handleAddOption}>Add</Button>
                        </div>
                    </div>
                </StyledFormField>
            </StyledMultipleQuestion>
        );
    }

}

export default MultipleQuestion;