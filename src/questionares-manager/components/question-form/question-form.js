import React from 'react';
import PropTypes from 'react-proptypes';
import styled from 'styled-components';
import StyledFormField from '../../../common/components/styled-form-field';
import Select from '../../../common/components/select';
import TextField from '../../../common/components/textfield';
import OpenQuestion from './open-question';
import YesNoQuestion from './yes-no-question';
import MultipleQuestion from './multiple-question';
import Question from '../../../common/models/question';

const StyledQuestionForm = styled.div`
    .select-type {
        margin-bottom: 10px;
    }
`;

class QuestionForm extends React.Component {

    static propTypes = {
        question: PropTypes.object,
        onChange: PropTypes.func,
        getOptions: PropTypes.func
    };

    constructor(props) {
        super(props);

        this.state = {
            question: this.initializeQuestion(props),
            questionTypes: [
                { value: 'open', text: 'Open' },
                { value: 'yes/no', text: 'Yes/No' },
                { value: 'multiple', text: 'Multiple' }
            ]
        };
    }

    componentWillReceiveProps(nextProps) {
        this.setState({ question: this.initializeQuestion(nextProps) });
    }

    initializeQuestion = ({ question }) => (question || new Question(null, 'open', '', ''));

    handleChange = (e) => {
        const question = { ...this.state.question, [e.target.name]: e.target.value  };
        this.setState({ question });
        this.props.onChange(question);
    };

    render() {
        return(
            <StyledQuestionForm>
                <StyledFormField>
                    <Select 
                        name="type" 
                        className="select-type"
                        value={this.state.question.type}
                        onChange={this.handleChange}>
                        {this.state.questionTypes.map(questionType =>
                            <option
                                key={questionType.value}
                                value={questionType.value}>{questionType.text}</option>)}
                    </Select>
                </StyledFormField>
                <StyledFormField>
                    <label>Question</label>
                    <TextField
                        name="question"
                        onChange={this.handleChange}
                        value={this.state.question.question} />
                </StyledFormField>
                {(this.state.question.type === 'open') &&
                    <OpenQuestion
                        value={this.state.question.correctAnswer}
                        onChange={this.handleChange} />}
                {(this.state.question.type === 'yes/no') &&
                    <YesNoQuestion
                        value={this.state.question.correctAnswer}
                        onChange={this.handleChange} />}
                {(this.state.question.type === 'multiple') &&
                    <MultipleQuestion
                        value={this.state.question.correctAnswer}
                        options={this.state.question.options}
                        onChange={this.handleChange} />}
            </StyledQuestionForm>
        );
    }

}

export default QuestionForm;