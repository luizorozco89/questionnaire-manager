import React from 'react';
import styled from 'styled-components';
import PropTypes from 'react-proptypes';
import StyledFormField from '../../../common/components/styled-form-field';
import TextField from '../../../common/components/textfield';

const StyledYesNoQuestion = styled.div`
    .answer-label {
        display: block;
        margin-bottom: 7px;
    }
`;

class YesNoQuestion extends React.Component {

    static propTypes = {
        value: PropTypes.string,
        onChange: PropTypes.func
    };

    state = { selectedOption: 'yes' };

    handleChange = (e) => {
        this.props.onChange(e);
        this.setState({ selectedOption: e.target.value });
    };

    isChecked = _value => {
        const { value } = this.props;
        return value === _value || this.state.selectedOption === _value;
    };

    render() {
        return(
            <StyledYesNoQuestion>
                <StyledFormField>
                    <label className="answer-label">Answer</label>
                    <input
                        type="radio"
                        name="correctAnswer"
                        value="yes"
                        checked={this.isChecked('yes')}
                        onChange={this.handleChange} />
                    <label htmlFor="yes">Yes</label>
                    <input
                        type="radio"
                        name="correctAnswer"
                        value="no"
                        checked={this.isChecked('no')}
                        onChange={this.handleChange} />
                    <label htmlFor="no">No</label>
                </StyledFormField>
            </StyledYesNoQuestion>
        );
    }
}

export default YesNoQuestion;