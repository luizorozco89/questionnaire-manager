import React from 'react';
import PropTypes from 'react-proptypes';
import StyledFormField from '../../../common/components/styled-form-field';
import TextField from '../../../common/components/textfield';

class OpenQuestion extends React.Component {

    static propTypes = {
        value: PropTypes.string,
        onChange: PropTypes.func
    };

    render() {

        const { value, onChange } = this.props;

        return(
            <StyledFormField>
                <label>Answer</label>
                <TextField
                    name="correctAnswer"
                    value={value}
                    onChange={onChange} />
            </StyledFormField>
        );
    }
}

export default OpenQuestion;