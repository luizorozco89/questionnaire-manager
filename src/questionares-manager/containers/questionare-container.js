import React, { Component } from "react";
import PropTypes from "react-proptypes";
import { connect } from "react-redux";
import styled from "styled-components";
import Toolbar from "../../common/components/toolbar/toolbar";
import { setCurrentTitle } from "../../common/actions/app-actions";
import { replaceInList } from "../../common/util";
import QuestionareForm from "../components/questionare-form/questionare-form";
import Drawer from "../../common/components/drawer/drawer";
import Questionare from "../../common/models/questionare";
import QuestionaresListItem from "../components/questionares-list/questionares-list-item";
import ConfirmAlert from "../../common/components/confirm-alert/confirm-alert";

const StyledQuestionareContainer = styled.div``;

class QuestionareContainer extends Component {
  static propTypes = {
    setCurrentTitle: PropTypes.func
  };

  constructor(props) {
    super(props);

    this.state = {
      showNewQuestionare: false,
      selectedQuestionare: this.getResetQuestionare(),
      questionares: [],
      filteredQuestionnaires: [],
      isFilteringBySearch: false,
      isFilteringByStatus: false,
      showConfirmAlert: false,
      filterSearchValue: "",
      filterStatusValue: ""
    };
  }

  componentDidMount() {
    this.props.setCurrentTitle("Questionare");
  }

  getResetQuestionare = () => new Questionare("", "", "created");

  handleOpenNewQuestionareDrawer = () => {
    this.setState({ showNewQuestionare: true });
  };

  handleCloseNewQuestionareDrawer = () => {
    this.setState({
      showNewQuestionare: false,
      selectedQuestionare: this.getResetQuestionare()
    });
  };

  handleQuestionareSelected = selectedQuestionare => {
    this.setState({ showNewQuestionare: true, selectedQuestionare });
  };

  handleSearch = searchInput => {
    this.setState({ filterSearchValue: searchInput });
    if (searchInput !== "") {
      this.setState({ isFilteringBySearch: true });
      const filteredQuestionnaireList = this.state.questionares.filter(q =>
        q.name.toLowerCase().includes(searchInput.toLowerCase())
      );
      this.setState({ filteredQuestionnaires: filteredQuestionnaireList });
    } else {
      if (this.state.filterSearchValue !== "") {
        this.handleFilterByStatus(this.state.filterStatusValue);
      }
      this.setState({ isFilteringBySearch: false });
    }
  };

  handleFilterByStatus = status => {
    this.setState({ filterStatusValue: status });
    if (status !== "") {
      const tempArr = this.state.questionares.filter(q => q.status === status);
      this.setState({ filteredQuestionnaires: tempArr });
      this.setState({ isFilteringByStatus: true });
    } else {
      if (this.state.filterSearchValue !== "") {
        this.handleSearch(this.state.filterSearchValue);
      }
      this.setState({ isFilteringByStatus: false });
    }
  };

  handleEvent = e => {
    switch (e.type) {
      case "on_add_new":
        this.handleOpenNewQuestionareDrawer();
        break;
      case "on_filter":
        this.handleFilterByStatus(e.value);
        break;
      case "on_search":
        this.handleSearch(e.value);
        break;
      default:
        console.log("Event =>", e);
    }
  };

  handleChangeQuestionare = selectedQuestionare => {
    this.setState({ selectedQuestionare });
  };

  isValidQuestionare = questionare => {
    var isValid = questionare ? true : false;
    if (
      (questionare && questionare.name.trim() === "") ||
      (questionare && questionare.description.trim() === "") ||
      (questionare && questionare.status.trim() === "")
    ) {
      isValid = false;
    }
    return isValid;
  };

  handleSaveQuestionare = () => {
    let questionares = this.state.questionares;
    if (this.state.selectedQuestionare.id) {
      questionares = replaceInList(
        questionares,
        this.state.selectedQuestionare,
        "id"
      );
    } else {
      questionares.push({
        ...this.state.selectedQuestionare,
        id: questionares.length + 1,
        date: new Date()
      });
    }
    this.setState({
      questionares,
      selectedQuestionare: this.getResetQuestionare(),
      filteredQuestionnaires: questionares
    });
  };

  handleSaveQuestion = question => {
    const { selectedQuestionare } = this.state;
    selectedQuestionare.saveQuestion(question);
    this.setState({ selectedQuestionare });
  };

  handleRemoveQuestionare = selectedQuestionare => {
    this.setState({ showConfirmAlert: true, selectedQuestionare });
  };

  handleDeleteQuestionare = parQuestionare => {
    const questionares = this.state.questionares.filter(
      questionare => questionare.id !== parQuestionare.id
    );
    this.setState({ questionares, showConfirmAlert: false });
  };

  handleDeleteQuestionareResponse = response => {
    if (response) {
      this.handleDeleteQuestionare(this.state.selectedQuestionare);
    } else {
      this.setState({
        showConfirmAlert: false,
        selectedQuestionare: this.getResetQuestionare()
      });
    }
  };

  render() {
    const newQuestionareOkButton = {
        text: "Create",
        disabled: !this.isValidQuestionare(this.state.selectedQuestionare),
        callback: this.handleSaveQuestionare
      },
      newQuestionareCancelButton = {
        text: "Cancel",
        callback: this.handleCloseNewQuestionareDrawer
      };

    const { showNewQuestionare, selectedQuestionare } = this.state;

    const getList = () => {
      return this.state.isFilteringBySearch || this.state.isFilteringByStatus
        ? this.state.filteredQuestionnaires
        : this.state.questionares;
    };

    return (
      <StyledQuestionareContainer>
        <Toolbar
          context="questionares"
          onEvent={this.handleEvent}
          filterOptions={[
            { text: "Created", value: "created" },
            { text: "Ready", value: "ready" },
            { text: "Answered", value: "answered" },
            { text: "Done", value: "done" }
          ]}
        />
        <div>
          {getList().map(questionare => (
            <QuestionaresListItem
              key={questionare.id}
              onSelect={this.handleQuestionareSelected}
              questionare={questionare}
              onRemoveQuestionare={this.handleRemoveQuestionare}
            />
          ))}
        </div>
        <Drawer
          show={showNewQuestionare}
          onClose={this.handleCloseNewQuestionareDrawer}
          okBtn={newQuestionareOkButton}
          cancelBtn={newQuestionareCancelButton}
          title="New questionare"
        >
          <QuestionareForm
            questionare={selectedQuestionare}
            onChange={this.handleChangeQuestionare}
            onSaveQuestion={this.handleSaveQuestion}
          />
        </Drawer>
        <ConfirmAlert
          show={this.state.showConfirmAlert}
          title="Deleting questionare"
          message={`Are you sure you want to delete questionare: ${this.state
            .selectedQuestionare && this.state.selectedQuestionare.name}?`}
          onClose={this.handleDeleteQuestionareResponse}
          okBtnText="Delete"
          cancelBtnText="Cancel"
        />
      </StyledQuestionareContainer>
    );
  }
}

//---------------------------------------------------------------

const mapDispatchToProps = dispatch => {
  return {
    setCurrentTitle: appTitle => {
      dispatch(setCurrentTitle(appTitle));
    }
  };
};

export default connect(
  null,
  mapDispatchToProps
)(QuestionareContainer);
