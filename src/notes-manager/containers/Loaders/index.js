import React from "react";
import ContentLoader from "react-content-loader";

const NotesListLoader = () => (
  <ContentLoader 
    speed={2}
    width={1000}
    height={1000}
    viewBox="0 0 1000 1000"
    backgroundColor="#f3f3f3"
    foregroundColor="#aeaeae"
  >
    <rect x="1" y="51" rx="0" ry="0" width="109" height="28" /> 
    <rect x="1" y="114" rx="0" ry="0" width="834" height="90" /> 
    <rect x="523" y="50" rx="0" ry="0" width="311" height="28" /> 
    <rect x="1" y="228" rx="0" ry="0" width="834" height="90" /> 
    <rect x="1" y="339" rx="0" ry="0" width="832" height="90" /> 
    <rect x="1" y="453" rx="0" ry="0" width="830" height="90" />
  </ContentLoader>
);

export default NotesListLoader;
