import React, { Component } from "react";
import PropTypes from "react-proptypes";
import { connect } from "react-redux";
import styled from "styled-components";
import ConfirmAlert from "../../common/components/confirm-alert/confirm-alert";
import Drawer from "../../common/components/drawer/drawer";
import Toolbar from "../../common/components/toolbar/toolbar";
import NotesListItem from "../components/notes-list/notes-list-item";
import NoteForm from "../components/note-form/note-form";
import NoteDetails from "../components/note-details/note-details";
import NotesListLoader from "./Loaders";

import {
  loadNotes,
  setShowNewNoteDrawer,
  saveNote,
  setFilterBy,
  setSearchBy,
  selectNote,
  setShowNoteDetailsDrawer,
  formNoteChange,
  setEditMode,
  deleteNote,
  setShowConfirmModal
} from "../actions/notes-list-actions";

import { setCurrentTitle } from "../../common/actions/app-actions";

const StyledNotesList = styled.div`
  .notes-list-container {
    margin-top: 20px;
  }
`;

class NotesListContainer extends Component {
  static propTypes = {
    notes: PropTypes.array,
    loadNotes: PropTypes.func,
    notesLoading: PropTypes.bool,

    setShowNewNoteDrawer: PropTypes.func,
    showNewNoteDrawer: PropTypes.bool,
    newNote: PropTypes.object,

    saveNote: PropTypes.func,
    setFilterBy: PropTypes.func,
    filterByStatus: PropTypes.string,
    setSearchBy: PropTypes.func,
    searchInput: PropTypes.string,

    selectNote: PropTypes.func,
    selectedNote: PropTypes.object,

    setShowNoteDetailsDrawer: PropTypes.func,
    showNoteDetailsDrawer: PropTypes.bool,

    formNoteChange: PropTypes.func,

    setEditMode: PropTypes.func,
    isEditMode: PropTypes.bool,

    currentNoteEditing: PropTypes.object,

    deleteNote: PropTypes.func,
    showDeleteConfirmModal: PropTypes.bool,
    setShowConfirmModal: PropTypes.func,

    isNoteValid: PropTypes.bool,

    setCurrentTitle: PropTypes.func
  };

  componentDidMount() {
    this.props.setCurrentTitle("Notes manager");
    this.props.loadNotes();
  }

  handleOpenNewNoteDrawer = () => {
    this.props.setShowNewNoteDrawer(true);
  };

  handleCloseNewNoteDrawer = () => {
    this.props.setShowNewNoteDrawer(false);
  };

  handleNoteSelected = note => {
    this.props.setShowNoteDetailsDrawer(true, note);
    //this.props.selectNote(note);
  };

  handleCloseNoteDetails = () => {
    this.props.setShowNoteDetailsDrawer(false, null);
    if (this.props.isEditMode) this.props.setEditMode(false);
  };

  isValidNote = note => {
    var isValid = note ? true : false;
    if (
      (note && note.title.trim() === "") ||
      (note && note.description.trim() === "") ||
      (note && note.status.trim() === "")
    ) {
      isValid = false;
    }
    return isValid;
  };

  handleRemoveNote = note => {
    this.props.setShowConfirmModal(true, note);
    //this.handleDeleteNoteResponse(true, note);
  };

  handleDeleteNoteResponse = response => {
    //response => true || false
    if (response) {
      this.props.deleteNote(this.props.selectedNote);
    } else {
      this.props.setShowConfirmModal(false, null);
    }
  };

  handleSearch = searchInput => {
    this.props.setSearchBy(searchInput);
  };

  handleFilterByStatus = status => {
    this.props.setFilterBy(status);
  };

  handleNoteFormChange = newNote => {
    this.props.formNoteChange(newNote);
  };

  handleSaveNote = () => {
    if (this.isValidNote(this.props.newNote)) {
      this.props.saveNote(this.props.newNote);
    }
  };

  handleEditSave = () => {
    if (this.props.isEditMode) {
      this.props.saveNote(this.props.currentNoteEditing);
      this.props.setShowNoteDetailsDrawer(false);
    } else {
      this.props.setEditMode(true);
    }
  };

  handleSetEdit = () => {
    this.props.setEditMode(true);
  };

  filterNotes = item => {
    const regex = new RegExp((this.props.searchInput || "").toLowerCase(), "g");

    //If no searchInput && no filterByStatus => include all
    if (!this.props.filterByStatus && !this.props.searchInput) {
      return true;
    }

    //If no searchInput && no filterByStatus => include all
    if (this.props.filterByStatus && this.props.searchInput) {
      return (
        item.status === this.props.filterByStatus &&
        regex.test(item.title.toLowerCase())
      );
    }

    if (this.props.filterByStatus) {
      return item.status === this.props.filterByStatus;
    }

    if (this.props.searchInput) {
      return regex.test(item.title.toLowerCase());
    }
  };

  handleEvent = e => {
    switch (e.type) {
      case "on_add_new":
        this.handleOpenNewNoteDrawer();
        break;
      case "on_filter":
        this.handleFilterByStatus(e.value);
        break;
      case "on_search":
        this.handleSearch(e.value);
        break;
      default:
        console.log(e);
    }
  };

  render() {
    const {
      notes,
      notesLoading,
      showNewNoteDrawer,
      newNote,
      showNoteDetailsDrawer,
      isEditMode,
      selectedNote,
      currentNoteEditing,
      showDeleteConfirmModal
    } = this.props;

    const newNoteOkBtn = {
        text: "Create",
        disabled: !this.isValidNote(newNote),
        callback: this.handleSaveNote
      },
      newNoteCancelBtn = {
        text: "Cancel",
        disabled: false,
        callback: this.handleCloseNewNoteDrawer
      },
      noteDetailsEditBtn = {
        text: isEditMode ? "Save" : "Edit",
        disabled: !isEditMode ? false : !this.isValidNote(currentNoteEditing),
        callback: this.handleEditSave
      },
      noteDetailsCancelBtn = {
        text: "Cancel",
        disabled: false,
        callback: this.handleCloseNoteDetails
      };

    return (
      notesLoading ? <NotesListLoader /> :
      <StyledNotesList>
        {console.log("notesLoading => ", notesLoading)}
        <Toolbar
          context="notes"
          onEvent={this.handleEvent}
          filterOptions={[
            { text: "Pending", value: "pending" },
            { text: "Done", value: "done" }
          ]}
        />

        <div className="notes-list-container">
          {notes.filter(this.filterNotes).map(note => (
            <NotesListItem
              key={note._id}
              note={note}
              onSelect={this.handleNoteSelected}
              onRemoveNote={this.handleRemoveNote}
            />
          ))}
        </div>

        <Drawer
          show={showNewNoteDrawer}
          onClose={this.handleCloseNewNoteDrawer}
          title="New note"
          okBtn={newNoteOkBtn}
          cancelBtn={newNoteCancelBtn}
        >
          <NoteForm onChange={this.handleNoteFormChange} note={newNote} />
        </Drawer>

        <Drawer
          show={showNoteDetailsDrawer}
          title={
            isEditMode
              ? `Editing ${selectedNote && selectedNote.title}`
              : "Note details"
          }
          onClose={this.handleCloseNoteDetails}
          okBtn={noteDetailsEditBtn}
          cancelBtn={noteDetailsCancelBtn}
        >
          {isEditMode ? (
            <NoteForm
              onChange={this.handleNoteFormChange}
              note={currentNoteEditing || selectedNote}
              editMode={isEditMode}
            />
          ) : (
            <NoteDetails note={currentNoteEditing || selectedNote} />
          )}
        </Drawer>

        <ConfirmAlert
          show={showDeleteConfirmModal}
          title="Deleting Note"
          message={`Are you sure to delete note: ${selectedNote &&
            selectedNote.title} ?`}
          onClose={this.handleDeleteNoteResponse}
          okBtnText="Delete"
          cancelBtnText="Cancel"
        />
      </StyledNotesList>
    );
  }
}

//---------------------------------------------------------------

const mapStateToProps = state => {
  return {
    notes: state.notesList.notes,
    notesLoading: state.notesList.areNotesLoading,
    showNewNoteDrawer: state.notesList.showNewNoteDrawer,
    newNote: state.notesList.newNote,
    filterByStatus: state.notesList.filterByStatus,
    searchInput: state.notesList.searchInput,
    selectedNote: state.notesList.selectedNote,
    showNoteDetailsDrawer: state.notesList.showNoteDetailsDrawer,
    isEditMode: state.notesList.isEditMode,
    currentNoteEditing: state.notesList.currentNoteEditing,
    showDeleteConfirmModal: state.notesList.showDeleteConfirmModal,
    isNoteValid: state.notesList.isNoteValid
  };
};

const mapDispatchToProps = dispatch => {
  return {
    loadNotes: () => {
      dispatch(loadNotes());
    },
    setShowNewNoteDrawer: value => {
      dispatch(setShowNewNoteDrawer(value));
    },
    saveNote: note => {
      dispatch(saveNote(note));
    },
    setFilterBy: status => {
      dispatch(setFilterBy(status));
    },
    setSearchBy: inputSearch => {
      dispatch(setSearchBy(inputSearch));
    },
    selectNote: note => {
      dispatch(selectNote(note));
    },
    setShowNoteDetailsDrawer: (value, note) => {
      dispatch(setShowNoteDetailsDrawer(value, note));
    },
    formNoteChange: data => {
      dispatch(formNoteChange(data));
    },
    setEditMode: value => {
      dispatch(setEditMode(value));
    },
    deleteNote: note => {
      dispatch(deleteNote(note));
    },
    setShowConfirmModal: (value, note) => {
      dispatch(setShowConfirmModal(value, note));
    },
    setCurrentTitle: appTitle => {
      dispatch(setCurrentTitle(appTitle));
    }
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(NotesListContainer);
