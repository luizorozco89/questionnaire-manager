//Loading notes
export const NOTES_LOAD = 'NOTES_LOAD';
export const NOTES_LOAD_SUCCESS = 'NOTES_LOAD_SUCCESS';
export const NOTES_LOAD_FAILURE = 'LOAD_NOTES_FAILURE';

//Opening section for showing details of a note selected
export const SET_SHOW_NOTE_DETAILS_DRAWER = 'SET_SHOW_NOTE_DETAILS_DRAWER'; 

//Selecting a note
export const SELECT_NOTE = 'SELECT_NOTE';
export const SELECT_NOTE_SUCCESS = 'SELECT_NOTE_SUCCESS';
export const SELECT_NOTE_FAILURE = 'SELECT_NOTE_FAILURE';

//Updating a note
export const SET_EDIT_MODE = 'SET_EDIT_MODE';
export const UPDATE_NOTE_SUCCESS = 'UPDATE_NOTE_SUCCESS';

//Opening section for the creation of a new note
export const SET_SHOW_NEW_NOTE_DRAWER = 'SET_SHOW_NEW_NOTE_DRAWER';

//Saving note(previously created or new)
export const SAVE_NOTE = 'SAVE_NOTE';
export const SAVE_NOTE_SUCCESS = 'SAVE_NOTE_SUCCESS';
export const SAVE_NOTE_FAILURE = 'SAVE_NOTE_FAILURE';

//Capturing data changes from a form
export const NOTE_FORM_CHANGED = 'NOTE_FORM_CHANGED'; 

//Filering existing notes
export const SET_SEARCH_BY = 'SET_SEARCH_BY'; 

//Searching for notes
export const SET_FILTER_BY = 'SET_FILTER_BY'; 

//Opening section to confirm the deleting process of a note
export const SET_SHOW_CONFIRM_MODAL = 'SET_SHOW_CONFIRM_MODAL'; 

//Deleting a note
export const DELETE_NOTE = 'DELETE_NOTE';
export const DELETE_NOTE_SUCCESS = 'DELETE_NOTE_SUCCESS';
export const DELETE_NOTE_FAILURE = 'DELETE_NOTE_FAILURE';

