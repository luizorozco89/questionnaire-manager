import React from 'react';
import PropTypes from 'react-proptypes';
import styled from 'styled-components';

const StyledNoteForm = styled.div`
    .form-fields {
        &.hide {
            display: none;
        }

        margin-bottom: 20px;

        label {
            display: inline-block;
            margin-bottom: 5px;
        }

        input, textarea {
            width: calc(100% - 12px);
        }

        textarea {
            height: 100px;
            max-height: 100px;
            padding: 5px; 
        }
    }
`;

class NoteForm extends React.Component {

    static propTypes = {
        onChange: PropTypes.func,
        note: PropTypes.object,
        editMode: PropTypes.bool
    };

    constructor(props) {
        super(props);

        this.state = {
            note: props.note
        };
    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            note: nextProps.note
        });
    }

    handleChange = (e) => {
        /*const newNote = Object.assign({}, this.state.note, { [e.target.name]: e.target.value });*/
        const newNote = { ...this.state.note, [e.target.name]: e.target.value };
        this.props.onChange &&
            this.props.onChange(newNote);
    }

    render() {
    
        const { editMode } = this.props;

        return (
            <StyledNoteForm>
                <div className="form-fields">
                    <label>Title</label>
                    <br />
                    <input
                        type="text"
                        name="title"
                        value={(this.state.note && this.state.note.title) || ''}
                        onChange={this.handleChange} />
                </div>

                <div className="form-fields">
                    <label>Description</label>
                    <br />
                    <textarea
                        name="description"
                        value={(this.state.note && this.state.note.description) || ''}
                        onChange={this.handleChange}></textarea>
                </div>

                {editMode &&
                    <div className="form-fields">
                        <select
                            name="status"
                            value={(this.state.note && this.state.note.status) || ''}
                            onChange={this.handleChange}>
                            <option value="">--Select a status--</option>
                            <option value="pending">Pending</option>
                            <option value="done">Done</option>
                        </select>
                    </div>}
            </StyledNoteForm>
        );
        
    }

}

export default NoteForm;