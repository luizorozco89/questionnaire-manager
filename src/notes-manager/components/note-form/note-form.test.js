import React from 'react';
import NoteForm from './note-form';
import { shallow } from 'enzyme';

describe('<NoteForm />', () => {

    const baseProps = {
        onChange: () => {},
        note: {
            title: "note title",
            description: "note description",
            status: "note status"
        },
        editMode: false
    };

    const getWrapper = (customProps) => {
        const props = { ...baseProps, ...(customProps || {}) }
        return shallow(
            <NoteForm  {...props}/>
        );
    };

    it('should be defined', () => {
        const wrapper = getWrapper();
        expect(wrapper).toBeDefined(); 
    });

    it('should return a div', () => {
        const wrapper = getWrapper();
        expect(wrapper.type()).toEqual('div'); 
    });

    it('should have className="note-form"', () => {
        const wrapper = getWrapper(),
            props = wrapper.props();
        expect(props.className).toEqual('note-form');
    });
    
});