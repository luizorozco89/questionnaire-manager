import React from 'react';
import NotesDetails from './note-details';
import { shallow } from 'enzyme';

describe('<NotesDetails />', () => {

    const baseProps = {
        note: {
            title: "note title",
            description: "note description",
            status: "note status"
        }
    };

    const getWrapper = (customProps) => {
        const props = { ...baseProps, ...(customProps || {}) };
        return shallow(
            <NotesDetails {...props}/>
        );
    };

    it('should be defined', () => {
        const wrapper = getWrapper();
        expect(wrapper).toBeDefined(); 
    });

    it('should return a section', () => {
        const wrapper = getWrapper();
        expect(wrapper.type()).toEqual('section'); 
    });

    it('should have className="note-details"', () => {
        const wrapper = getWrapper(),
            props = wrapper.props();
        expect(props.className).toEqual('note-details');
    });
    
});