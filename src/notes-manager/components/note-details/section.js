import styled from 'styled-components';

const Section = styled.section`
    .date {
        color: #00a4d3;
    }

    .description {
        color: #858C9C;
    }

    .status {

        &.pending {
            color: #ffa834;
        }

        &.done {
            color: #72bb53;
        }

    }
`;

export default Section;