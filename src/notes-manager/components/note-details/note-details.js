import React from 'react';
import PropTypes from 'react-proptypes';
import moment from 'moment';
import styled from 'styled-components';

const StyledNotesDetails = styled.section`
    .date {
        color: #00a4d3;
    }

    .description {
        color: #858C9C;
    }

    .status {
        &.pending {
            color: #ffa834;
        }

        &.done {
            color: #72bb53;
        }
    }
`;

class NotesDetails extends React.Component {

    static propTypes = {
        note: PropTypes.object
    };
    
    render() {
        const { note } = this.props;

        return(
            <StyledNotesDetails>
                <p>{note && note.title}</p>
                <p className="description">{note && note.description}</p>
                <p className="date"><i className="fa fa-calendar" aria-hidden="true"></i> {note && moment(note.date).fromNow()}</p>
                <p className={`status ${note && note.status}`}><i className="fa fa-clock-o" aria-hidden="true"></i> {note && note.status}</p>
            </StyledNotesDetails>
        );
    }
}

export default NotesDetails;