import React from 'react';
import PropTypes from 'react-proptypes';
import Button from '../../../common/components/button';
import Textfield from '../../../common/components/textfield';
import Select from '../../../common/components/select';

class NotesListHeader extends React.Component {

    handleChange = (e) => {
        e.target.name === 'search' &&
            this.props.onSearch(e.target.value);

        e.target.name === 'status' &&
            this.props.onFilterBy(e.target.value);
    }

    render() {
        const { filterBy, search, onNewNoteBtnClick } = this.props;

        return (  
            <div className="notes-list-header">
                <Button
                    className="primary"
                    onClick={onNewNoteBtnClick}> 
                    <i className="fa fa-plus" aria-hidden="true" id="plus-icon"></i>New
                </Button>
                <div className="notes-options-right">
                    <span>Filter by </span>
                    <Select
                        name="status"
                        value={filterBy}
                        onChange={this.handleChange}>
                        <option value="">--Select a status--</option>
                        <option value="pending">Pending</option>
                        <option value="done">Done</option>
                    </Select>
                    <Textfield
                        type="text"
                        name="search"
                        value={search}
                        placeholder="Search notes..."
                        onChange={this.handleChange} />   
                </div>                
            </div>    
        );
    }

}

 NotesListHeader.propTypes = {
    filterBy: PropTypes.string,
    search: PropTypes.string,
    onNewNoteBtnClick: PropTypes.func,
    onSearch: PropTypes.func,
    onFilterBy: PropTypes.func
 };

export default NotesListHeader;