import React from 'react';
import NotesListHeader from './notes-list-header';
import { shallow } from 'enzyme';

describe('<NotesListHeader />', () => {

    const baseProps = {
        filterBy: 'filter by',
        search: 'search',
        onNewNoteBtnClick: () => {},
        onSearch: () => {},
        onFilterBy: () => {}
    };

    const getWrapper = (customProps) => {
        const props = { ...baseProps, ...(customProps || {}) };
        return shallow(
            <NotesListHeader {...props}/>
        );
    };

    it('should be defined', () => {
        const wrapper = getWrapper();
        expect(wrapper).toBeDefined(); 
    });

    it('should return a div', () => {
        const wrapper = getWrapper();
        expect(wrapper.type()).toEqual('div'); 
    });

    it('should have className="notes-list-header"', () => {
        const wrapper = getWrapper(),
            props = wrapper.props();
        expect(props.className).toEqual('notes-list-header');
    });
    
});