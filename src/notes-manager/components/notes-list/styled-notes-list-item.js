import styled from 'styled-components';

const StyledNotesListItem = styled.div`
    margin-bottom: 10px;
    padding: 10px;
    border: 1px solid #e0e0e0;
    border-radius: 3px;
    background-color: #fff;
    position: relative;

    p {
        margin-top: 0;

        .title {
            margin-right: 20px;
            max-width: 45%;
            white-space: nowrap;
            overflow: hidden;
            text-overflow: ellipsis;
            display: inline-block; 
        }

        .date {
            background-color: #00a4d3;
            border-radius: 3px;
            border: 0px;
            font-size: 75%;
            padding: 2px 22px;
            text-align: center;
            color: #fff;
        }

    }

    > i {
        position: absolute;
        right: 10px;
        top: 10px;
        cursor: pointer;
    }

    div {
        margin-top: 15px;

        .description {
            color: #858C9C;
            max-width: 85%;
            display: inline-block;
            font-size: 13px;
            font-weight: 500;
        }

        .status {
            float: right;
            text-transform: uppercase;
            font-size: 14px;
            position: absolute;
            bottom: 10px;
            right: 10px;

            &.pending {
                color: #ffa834;
            }

            &.done {
                color: #72bb53;
            }

            i {
                margin-right: 5px;
            }
        }

    }

`;

export default StyledNotesListItem;