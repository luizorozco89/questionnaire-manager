import React from 'react';
import NotesListItem from './notes-list-item';
import { shallow } from 'enzyme';

describe('<NotesListItem />', () => {

    const baseProps = {
        note: {
            title: "Note title",
            description: "Note description",
            status: "Note status",
        },
        onSelect: () => {},
        onRemoveNote: () => {}
    };

    const getWrapper = (customProps) => {
        const props = { ...baseProps, ...(customProps || {}) };
        return shallow(
            <NotesListItem {...props} />
        );
    };

    it('should be defined', () => {
        const wrapper = getWrapper();
        expect(wrapper).toBeDefined(); 
    });

    it('should return a div', () => {
        const wrapper = getWrapper();
        expect(wrapper.type()).toEqual('div'); 
    });

    it('should have className="note-list-item"', () => {
        const wrapper = getWrapper(),
            props = wrapper.props();
        expect(props.className).toEqual('note-list-item');
    });
    
});