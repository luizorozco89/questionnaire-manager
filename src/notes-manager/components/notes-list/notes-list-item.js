
import React from 'react';
import PropTypes from 'react-proptypes';
import moment from 'moment';
import StyledNotesListItem from './styled-notes-list-item';
import { toCapital } from '../../../common/util';

class NotesListItem extends React.Component {

    static propTypes = {
        note: PropTypes.object,
        onSelect: PropTypes.func,
        onRemoveNote: PropTypes.func
    };

    handleSelect = () => {
        this.props.onSelect &&
            this.props.onSelect(this.props.note);
    }

    handleRemoveNote = (e) => {
        e.stopPropagation();
        this.props.onRemoveNote &&
            this.props.onRemoveNote(this.props.note);
    }

    render() {

        const { note } = this.props;
        
        return (
           <StyledNotesListItem onClick={this.handleSelect}>
                <p>
                    <label className="title">{toCapital(note.title)}</label>
                    <label className="date">{moment(note.date).fromNow()}</label>
                </p>
                <i onClick={this.handleRemoveNote} className="fa fa-times" aria-hidden="true"></i>
                <div>
                    <label className="description">{toCapital(note.description)}</label>
                    <span className={`status ${note.status}`}>
                        <i className="fa fa-clock-o" aria-hidden="true"></i>
                        {note.status}
                    </span>
                </div>
            </StyledNotesListItem>
        );
    }

}

export default NotesListItem;



  
