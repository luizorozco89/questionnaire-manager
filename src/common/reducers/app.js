import { SET_CURRENT_TITLE } from '../actions/types';

const INITIAL_STATE = {
    appTitle: 'Notes manager'
};

function appReducer(state = INITIAL_STATE, action) {
    switch(action.type) {
        case SET_CURRENT_TITLE:
            return { ...state, appTitle: action.payload };
        default:
            return state;
    }
}

export default appReducer;