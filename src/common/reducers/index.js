import { combineReducers } from 'redux';
import app from './app';
import questionare from '../../questionares-manager/reducers/questionare';
import notesList from '../../notes-manager/reducers/notes-list';

export default combineReducers({
    app,
    questionare,
    notesList
});