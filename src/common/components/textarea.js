import styled from 'styled-components';

const Textarea = styled.textarea`
    padding: 0 5px;
    border: 1px solid #e0e0e0;
    border-radius: 2px;
    height: 28px;
    font-size: 90%;
`;

export default Textarea;