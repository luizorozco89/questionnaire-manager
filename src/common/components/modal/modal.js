import React from 'react';
import PropTypes from 'react-proptypes';
import styled from 'styled-components';
import Overlay from '../overlay/overlay';

const StyledModal = styled.div`
    background: #fff;
    width: 500px;
    position: fixed;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
    border-radius: 3px;

    > header {
        padding: 15px;
        background-color: #F0F0F0;
        font-weight: bold;
        color: #989898;

        i {
            float: right;
            cursor: pointer;
        }
    }

    > section {
        padding: 20px;
    }
`;

class Modal extends React.Component {

    static propTypes = {
        show: PropTypes.bool,
        title: PropTypes.string,
        children: PropTypes.any,
        onClose: PropTypes.func
    };

    close = () => {
        this.props.onClose && this.props.onClose();
    }

    render() {

        const {
            show,
            title,
            children
        } = this.props;
        
        return (
            <Overlay show={show}>
                <StyledModal>
                    <header>
                        <div>
                            {title}
                            <i onClick={this.close} 
                                className="fa fa-times" 
                                aria-hidden="true"></i>
                        </div>
                    </header>
                    <section>
                        {children}
                    </section>
                </StyledModal>
            </Overlay>
        );
    }

}

export default Modal;