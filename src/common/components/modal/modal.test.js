import React from 'react';
import Modal from './modal';
import { shallow } from 'enzyme';

describe('<Modal />', () => {

    const baseProps = {
        show: false,
        title: 'Some title',
        message: 'Some msg',
        cancelBtn: {
            callback: () => {},
            text: "string"
        },
        okBtn: {
            text: '',
            callback: () => {}
        },
        onClose: () => {}
    };

    const getWrapper = (customProps) => {
        const props = { ...baseProps, ...(customProps || {}) };
        return shallow(
            <Modal {...props} />
        );
    };

    it('should be defined', () => {
        const wrapper = getWrapper();
        expect(wrapper).toBeDefined(); 
    });

    /*it('should have show=true', () => {
        const wrapper = getWrapper({ show: true }),
            props = wrapper.instance().props;
        expect(props.show).toEqual(true);
    });

    it('should have show=false', () => {
        const wrapper = getWrapper({ show: false }),
            props = wrapper.instance().props;
        expect(props.show).toEqual(false);
    });*/
    
});