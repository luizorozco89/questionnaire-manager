import styled from 'styled-components';

const StyledFormField = styled.div`
    label {
        font-size: 13px;
    }
    input[type="text"],
    input[type="password"],
    textarea,
    select {
        width: 100%;
        margin: 7px 0px;
        box-sizing: border-box;
    }
`;

export default StyledFormField;