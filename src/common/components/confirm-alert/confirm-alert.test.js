import React from 'react';
import ConfirmAlert from './confirm-alert';
import { shallow } from 'enzyme';

describe('<ConfirmAlert />', () => {

    const baseProps = {
        show: false,
        title: 'Some title',
        message: 'Some message',
        onClose: () => {},
        okBtnText: 'Ok button text',
        CancelBtnText: 'Cancel button text'
    };

    const getWrapper = (customProps) => {
        const props = { ...baseProps, ...(customProps || {}) };
        return shallow(
            <ConfirmAlert {...props} />
        );    
    };

    it('should be defined', () => {
        const wrapper = getWrapper();
        expect(wrapper).toBeDefined(); 
    });

    /*it('should have show=true', () => {
        const wrapper = getWrapper({show: true}),
            props = wrapper.instance().props;
        expect(props.show).toEqual(true);
    });

    it('should have show=false', () => {
        const wrapper = getWrapper({show: false}),
            props = wrapper.instance().props;
        expect(props.show).toEqual(false);
    });*/
    
});