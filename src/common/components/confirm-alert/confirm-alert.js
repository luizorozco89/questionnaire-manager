import React from 'react';
import PropTypes from 'react-proptypes';
import styled from 'styled-components';
import Overlay from '../overlay/overlay';
import Button from '../button';

const StyledConfirmAlert = styled.div`
    background: #fff;
    width: 500px;
    position: fixed;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
    border-radius: 3px;

    header {
        padding: 15px;

        i {
            float: right;
            cursor: pointer;
        }
    }

    section {
        padding: 15px 15px 30px 15px;

        p {
            margin: 0;
            color: #666;
            font-size: 14px;
            font-weight: 500;
        }
    }

    footer {
        background-color: #f0f0f0;
        padding: 15px;
        
        .cancel-btn {
            color: #666;
        }

        .ok-btn {
            float: right;
            background-color: #5bc0de; 
        }
    }
`;

class ConfirmAlert extends React.Component {

    static propTypes = {
        show: PropTypes.bool,
        title: PropTypes.string,
        message: PropTypes.string,
        onClose: PropTypes.func,
        okBtnText: PropTypes.string,
        cancelBtnText: PropTypes.string
    };

    ok = () => {
        this.props.onClose && this.props.onClose(true);
    };
          
    cancel = () => {
        this.props.onClose && this.props.onClose(false);
    };   

    render() {

        const { show, title, message, okBtnText, cancelBtnText } = this.props;

        return(
            <Overlay show={show}>
                <StyledConfirmAlert>
                    <header>
                        {title}
                        <i
                            onClick={this.cancel}
                            className="fa fa-times"
                            aria-hidden="true" />
                    </header>
                    <section>
                        <p>{message}</p> 
                    </section>
                    <footer>
                        <Button
                            className="cancel-btn"
                            onClick={this.cancel}>
                            {cancelBtnText || 'Cancel'}
                        </Button>
                        <Button
                            className="ok-btn"
                            onClick={this.ok}>
                            {okBtnText || 'Ok'}
                        </Button>
                    </footer>
                </StyledConfirmAlert>
            </Overlay>
        );
    }

}

export default ConfirmAlert;