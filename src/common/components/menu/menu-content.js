import React from 'react';
import PropTypes from 'react-proptypes';
import { Link } from "react-router-dom";

class MenuContent extends React.Component {
    render() {
        return(
            <div>
                <Link to={"/"}>Notes manager</Link><br/>
                <Link to={"/questionare"}>Questionare</Link>
            </div>
        );
    };
}

export default MenuContent;