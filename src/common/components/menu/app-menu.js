import React from 'react';
import PropTypes from 'react-proptypes';
import styled from 'styled-components';
import { Link } from "react-router-dom";

const Menu = styled.div`
    position: fixed;
    top: 55px;
    left: 0;
    background: #fff;
    border-right: 1px solid #e0e0e0;
    width: 200px;
    height: 100vh;
    z-index: 1;

    .links {
        display: inline-block;
        width: 100%;
        padding: 10px 20px;
        box-sizing: border-box;
        text-decoration: none;
        color: #666;

        &:hover {
            background: #e0e0e0;
            color: #00a4d3;
        }
    }
`;

class AppMenu extends React.Component {

    static propTypes = {
        show: PropTypes.bool,
        onOpenClose: PropTypes.func
    };

    handleOpenCloseMenu = () => {
        this.props.onOpenClose();
    };

    render() {

        const { show } = this.props;

        return(
            <Menu className="app-menu" style={{ display: show ? 'block' : 'none' }}>
                <section>
                    <Link
                        className="links"
                        onClick={this.handleOpenCloseMenu}
                        to="/">
                        Notes manager
                    </Link>
                    <Link
                        className="links"
                        onClick={this.handleOpenCloseMenu}
                        to="/questionare">
                        Questionare
                    </Link>
                </section>    
            </Menu>
        );
    }
}

export default AppMenu;