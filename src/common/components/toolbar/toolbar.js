import React from "react";
import PropTypes from "react-proptypes";
import styled from "styled-components";
import Button from "../button";
import Textfield from "../textfield";
import Select from "../select";

const StyledToolbar = styled.div`
  padding: 10px;

  .primary {
    i {
      padding-top: 2px;
      float: left;
    }
  }

  .options-right {
    float: right;

    select {
      margin-right: 4px;
    }
  }
`;

class Toolbar extends React.Component {
  static propTypes = {
    context: PropTypes.string,
    onEvent: PropTypes.func,
    filterOptions: PropTypes.array
  };

  state = {
    filter: "",
    search: ""
  };

  handleClick = e => {
    this.handleEvent(e, "on_add_new");
  };

  handleFilter = e => {
    this.handleEvent(e, "on_filter", e.target.value);
  };

  handleSearch = e => {
    this.handleEvent(e, "on_search", e.target.value);
  };

  handleEvent = (e, type, value) => {
    this.setState({
      [e.target.name]: value
    });
    const { onEvent = () => {} } = this.props;
    onEvent({
      type,
      value
    });
  };

  render() {
    const { context, filterOptions } = this.props;

    return (
      <StyledToolbar>
        <Button className="primary" onClick={this.handleClick}>
          <i className="fa fa-plus" />
          New
        </Button>
        <div className="options-right">
          <span>Filter by </span>
          <Select
            name="filter"
            value={this.state.filter}
            onChange={this.handleFilter}
          >
            <option value="">--Select--</option>
            {filterOptions.map(option => (
              <option key={option.value} value={option.value}>
                {option.text}
              </option>
            ))}
          </Select>
          <Textfield
            type="text"
            name="search"
            value={this.state.search}
            placeholder={`Search ${context}...`}
            onChange={this.handleSearch}
          />
        </div>
      </StyledToolbar>
    );
  }
}

export default Toolbar;
