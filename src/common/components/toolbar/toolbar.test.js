<main className="app">
    <AppHeader />
    <div className="app-content">
        <Switch>
            <Route exact path='/' component={NotesListContainer}/>
            <Route path='/questionare' component={QuestionareContainer}/>
        </Switch>
    </div>
</main>


<header className="app-header">
    <i
        className="fa fa-bars"
        aria-hidden="true"
        onClick={this.handleOpenCloseMenu}></i>
    <h1>{appTitle}</h1>
    <AppMenu
        show={this.state.showMenu}
        onOpenClose={this.handleOpenCloseMenu} />
</header>

const Overlay = ({ show, children }) => {
    return (
        <divOverlay className={`overlay ${show ? 'show' : ''}`}>
            {children}
        </divOverlay>
    );
};