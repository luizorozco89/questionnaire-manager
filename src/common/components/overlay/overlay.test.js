import React from 'react';
import Overlay from './overlay';
import { shallow } from 'enzyme';

describe('<Overlay />', () => {

    const baseProps = {
        show: false
    };

    const getWrapper = (customProps) => {
        const props = { ...baseProps, ...(customProps || {}) };
        return shallow(
            <Overlay {...props}>
                <p>Testing Overlay children</p>
            </Overlay>
        );
    }

    it('should be defined', () => {
        const wrapper = getWrapper();
        expect(wrapper).toBeDefined(); 
    });

    it('should return a div', () => {
        const wrapper = getWrapper();
        expect(wrapper.type()).toEqual('div'); 
    });

    /*it('should have show=false', () => {
        const wrapper = getWrapper({ show: false }),
            props = wrapper.props();
            console.log(props);
        expect(props.show).toEqual(false);
    });

    it('should have show=true', () => {
        const wrapper = getWrapper({show: true}),
            props = wrapper.props();
        expect(props.show).toEqual(true);
    });*/

    it('should have className="overlay show"', () => {
        const wrapper = getWrapper({show: true}),
            props = wrapper.props();
        expect(props.className).toEqual('overlay show');
    });

    it('should have className="overlay "', () => {
        const wrapper = getWrapper({show: false}),
            props = wrapper.props();
        expect(props.className).toEqual('overlay ');
    });

});