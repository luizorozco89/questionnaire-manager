import React from 'react';
import PropTypes from 'react-proptypes';
import styled from 'styled-components';

const StyledOverlay = styled.div`
    position: fixed;
    width: 100%;
    height: 100%;
    left: 0;
    top: 0;
    background: rgba(0, 0, 0, 0.2);
    z-index: 1;
    display: ${props => props.show ? 'inline-block' : 'none'};
`;

const Overlay = ({ show, children }) => {
    return (
        <StyledOverlay show={show}>
            {children}
        </StyledOverlay>
    );
};

Overlay.propTypes = {
    show: PropTypes.bool,
    children:PropTypes.any
};

export default Overlay;