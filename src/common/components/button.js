import styled from 'styled-components';

const Button = styled.button`
    width: 100px;
    border-radius: 3px;
    border: 0px;
    font-size: 75%;
    color: #666;
    padding: 8px;
    text-align: center;
    height: 30px;
    cursor: pointer;

    &.primary {
        background-color: #5bc0de;
        color: #fff;
    }

    &.danger {
        background-color: #cc3300;
        color: #fff;
    }
`;

export default Button;