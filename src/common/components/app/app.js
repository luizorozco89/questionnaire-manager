import React from 'react';
import { Switch, Route } from 'react-router-dom';
import styled from 'styled-components';
import AppHeader from './app-header';
import NotesListContainer from '../../../notes-manager/containers/notes-list-container';
import QuestionareContainer from '../../../questionares-manager/containers/questionare-container';
import './app.css';

const StyledAppMain = styled.main`
    display: flex;
    flex-direction: column;
    height: 100vh;

    .app-content {
        //padding: 50px 50px;
        background-color: #f5f5f5;
        padding-top: 50px;
        width: 811px;
        margin: 0 auto;
        height: calc(100% - 54px);
    }
`;

class App extends React.Component {
    
    render() {
        return (
            <StyledAppMain>
                <AppHeader />
                <div className="app-content">
                    <Switch>
                        <Route exact path='/' component={NotesListContainer}/>
                        <Route path='/questionare' component={QuestionareContainer}/>
                    </Switch>
                </div>
            </StyledAppMain>
        );
    }

}

export default App;
