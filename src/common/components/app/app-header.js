import React from 'react';
import PropTypes from 'react-proptypes';
import { connect } from 'react-redux';
import styled from 'styled-components';
import AppMenu from '../menu/app-menu';

const Header = styled.header`
    padding: 15px 50px;
    background-color: #fff;
    color: #858C9C;

    i {
        float: left;
        font-size: 150%;
        cursor: pointer;
    }

    h1 {
        font-size: 130%;
        margin: 0 0 0 35px;
    }
`;

class AppHeader extends React.Component {

    static propTypes = {
        appTitle: PropTypes.string
    };

    state = { showMenu: false };

    handleOpenCloseMenu = () => {
        this.setState({
            showMenu: !this.state.showMenu
        });
    };

    render() {

        const { appTitle } = this.props;

        return (
            <Header>
                <i
                    className="fa fa-bars"
                    aria-hidden="true"
                    onClick={this.handleOpenCloseMenu}></i>
                <h1>{appTitle}</h1>
                <AppMenu
                    show={this.state.showMenu}
                    onOpenClose={this.handleOpenCloseMenu} />
            </Header>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        appTitle: state.app.appTitle
    }
};

export default connect(mapStateToProps)(AppHeader);