import React from 'react';
import App from './app';
import { shallow } from 'enzyme';

describe('<App />', () => {

    const getWrapper = () => {
        return shallow(
            <App />
        );
    };

    it('should be defined', () => {
        const wrapper = getWrapper();
        expect(wrapper).toBeDefined(); 
    });

    it('should return a div', () => {
        const wrapper = getWrapper();
        expect(wrapper.type()).toEqual('div'); 
    });

    it('should have className="app"', () => {
        const wrapper = getWrapper(),
            props = wrapper.props();
        expect(props.className).toEqual('app');
    });
    
});