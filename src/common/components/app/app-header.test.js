import React from 'react';
import AppHeader from './app-header';
import { shallow } from 'enzyme';

describe('<AppHeader />', () => {

    const getWrapper = () => {
        return shallow(
            <AppHeader />
        );
    };

    it('should be defined', () => {
        const wrapper = getWrapper();
        expect(wrapper).toBeDefined(); 
    });

    it('should return a header', () => {
        const wrapper = getWrapper();
        expect(wrapper.type()).toEqual('header'); 
    });

    it('should have className="app-header"', () => {
        const wrapper = getWrapper(),
            props = wrapper.props();
        expect(props.className).toEqual('app-header');
    });
    
});