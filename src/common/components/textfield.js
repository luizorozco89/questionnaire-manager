import styled from 'styled-components';

const Textfield = styled.input.attrs({ type: 'text' })`
    font-size: 90%;
    height: 28px;
    line-height: 28px;
    border-radius: 2px;
    border: 1px solid #e0e0e0;
    padding: 0 10px;
`;

export default Textfield;