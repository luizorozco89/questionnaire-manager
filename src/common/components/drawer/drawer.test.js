import React from 'react';
import Drawer from './drawer';
import { shallow } from 'enzyme';

describe('<Drawer />', () => {

    const baseProps = {
        show: false,
        onClose: () => {},
        title: 'Some title',
        okBtn: {
            disabled: false,
            callback: () => {},
            text: "ok Button text"
        },
        cancelBtn: {
            disabled: false,
            callback: () => {},
            text: "Cancel button text"
        },
        editingTitle: 'Editing title text'
    };

    const getWrapper = (customProps) => {
        const props = { ...baseProps, ...(customProps || {}) };
        return shallow(
            <Drawer {...props}>
                <p>Testing Drawer children</p>
            </Drawer>
        );
    };

    it('should be defined', () => {
        const wrapper = getWrapper();
        expect(wrapper).toBeDefined(); 
    });

    /*it('should have show=true', () => {
        const wrapper = getWrapper(true),
            props = wrapper.instance().props;
        expect(props.show).toEqual(true);
    });

    it('should have show=false', () => {
        const wrapper = getWrapper(false),
            props = wrapper.instance().props;
        expect(props.show).toEqual(false);
    });*/
    
});