import React from 'react';
import PropTypes from 'react-proptypes';
import styled from 'styled-components';
import Overlay from '../overlay/overlay';
import Button from '../button';

const StyledDrawer = styled.div`
    display: inline-block !important;
    width: 400px;
    padding: 0px;
    display: none;
    border: 1px solid #e0e0e0;
    background: #fff;
    position: fixed;
    top: 0;
    right: 0;
    height: 100vh;

    > header {
        background-color: #f5f5f5;
        height: 50px;
        padding: 0 20px;
        line-height: 50px;

        i {
           float: right;
            line-height: 50px;
            cursor: pointer; 
        }
    } 
    
    > section {
        height: calc(100% - 140px);
        padding: 20px;
        overflow: auto;
    }

    > footer {
        background-color: #f5f5f5;
        border: 1px solid #e0e0e0;
        height: 50px;
        padding: 0 20px;
        line-height: 50px;
        position: relative;
        top: 41px;

        button {
            position: absolute;
            top: 10px;
        }

        .close-btn {
            left: 20px;
            background-color: #fff;
            color: #666;
            border: 1px solid #e0e0e0;
        }

        .ok-btn {
            right: 20px;
            background-color: #5bc0de;

            &.disabled {
                opacity: 0.5;
                cursor: no-drop;
            }
        }
    } 
`;

class Drawer extends React.Component {

    static propTypes = {
        show: PropTypes.bool,
        onClose: PropTypes.func,
        title: PropTypes.string,
        okBtn: PropTypes.object,
        cancelBtn: PropTypes.object,
        children: PropTypes.any,
        editingTitle: PropTypes.string
    };

    handleClose = () => {
        const { onClose = () => {} } = this.props;
        onClose();
    }

    render() {
        const {
            show,
            title,
            okBtn,
            cancelBtn,
            children
        } = this.props;

        return (
            <Overlay show={show}>
                <StyledDrawer>
                    <header>
                        <label>{title}</label>
                        <i
                            className="fa fa-close"
                            onClick={this.handleClose}></i>
                    </header>

                    <section>
                        {children}
                    </section>

                    <footer>
                        {cancelBtn &&
                            <Button
                                className="close-btn"
                                disabled={cancelBtn.disabled}
                                onClick={cancelBtn.callback}>
                                {cancelBtn.text}
                            </Button>}
                        
                        {okBtn &&
                            <Button
                                className={`ok-btn ${okBtn.disabled ? 'disabled' : ''}`}
                                disabled={okBtn.disabled}
                                onClick={okBtn.callback}>
                                {okBtn.text}
                            </Button>}
                    </footer>
                </StyledDrawer> 
            </Overlay>
        );
    }

}

export default Drawer;