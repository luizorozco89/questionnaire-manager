import Question from "./question";
import { replaceInList } from "../util";

class Questionare {
  constructor(name, description, status) {
    this.name = name;
    this.description = description;
    this.status = status;

    var _questions = [];
    var _counter = 1;

    this.getQuestions = () => _questions;
    this.setQuestions = questions => (_questions = questions);
    this.getCounter = () => _counter;
    this.setCounter = counter => (_counter = counter);
  }

  saveQuestion = question => {
    if (question.id) {
      this.setQuestions(replaceInList(this.getQuestions(), question, "id"));
    } else {
      const counter = this.getCounter();
      question.id = counter;
      this.getQuestions().push(
        new Question(
          question.id,
          question.type,
          question.question,
          question.correctAnswer,
          question.options
        )
      );
      this.setCounter(counter + 1);
    }
  };

  answerQuestion = (id, userAnswer) => {
    const questionToAnswer = this.getQuestions().find(
      question => question.id === id
    );
    questionToAnswer && questionToAnswer.answer(userAnswer);
  };

  evaluate = () => {
    const questions = this.getQuestions();
    const percentage =
      (questions.filter(question => question.isCorrect()).length /
        questions.length) *
      100;
    console.log(
      `Your score is ${percentage}%; You ${
        percentage >= 70 ? "Passed" : "Failed"
      }`
    );
  };
}

export default Questionare;
