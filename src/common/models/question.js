export default class Question {

    constructor(id, type, question, correctAnswer, options) {
        this.id = id;
        this.type = type;
        this.question = question;
        this.correctAnswer = correctAnswer;
        this.options = options;

        var _userAnswer = null;

        this.getUserAnswer = () => _userAnswer;
        this.setUserAnswer = (userAnswer) => _userAnswer = userAnswer;
    }

	answer(answer) {
		this.setUserAnswer(answer);
	}

	print() {
		switch(this.type) {
            case 'open': this.printOpen(); break;
			case 'yes/no': this.printYesNo(); break;
			case 'multiple': this.printMultiple(); break;
		}
	}

	isCorrect() {
		return this.correctAnswer === this.getUserAnswer();
	}

	printOpen() {
		console.log(`${this.id}.- ${this.question}`);
	}

	printYesNo() {
		console.log(`${this.id}.- ${this.question}`);
		console.log('Yes/No?');
	}

	printMultiple() {
		console.log(`${this.id}.- ${this.question}`);
		Object.keys((this.options || {}))
			.map(key => console.log(`${key}: ${this.options[key]}`));
	}

}