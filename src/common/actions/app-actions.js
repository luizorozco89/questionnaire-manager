import { SET_CURRENT_TITLE } from '../actions/types';

export const setCurrentTitle = (title) => {
    return {
        type: SET_CURRENT_TITLE,
        payload: title
    };
};

