import { createStore, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';
import rootReducer from '../reducers';

const configureStore = (initialState) => {
    const enhancer = compose(applyMiddleware(thunk)),
        store = createStore(rootReducer, initialState, enhancer);

    return store;
};

export default configureStore;