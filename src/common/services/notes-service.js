// ARQUITECTURA CLIENTE - SERVIDOR
// HTTP -> [GET, POST, PUT, DELETE]
// CLIENT => REQUEST
// SERVER => RESPONSE
// REST => REPRESENTATIONAL STATE TRANSFER
//RESOURCE => (NOTE MODEL): (SERVER SIDE)
// GET => /notes
// GET => /notes/:id
// POST => /notes
// PUT => /notes/:id
// DELETE => /notes/:id
// CRUD => CREATE, READ, UPDATE, DELETE
const NotesService = {
  //READ => [GET]
  getNotes: async () => {
    try {
      const response = await fetch("http://localhost:3001/notes", {
        mode: "cors"
      });
      return response.json();
    } catch (error) {
      return false;
    }

    /* return fetch('http://localhost:3001/notes', {
                mode: 'cors'
            })
            .then(response => {
                return response.json();
            }); */
  },
  //READ => [GET]
  getNoteById: async noteId => {
    try {
      const response = await fetch(`http://localhost:3001/notes/${noteId}`);
      return response.json();
    } catch(error) {
      return false;
    }

    /* return fetch(`http://localhost:3001/notes/${noteId}`).then(response => {
      return response.json();
    }); */

  },
  //CREATE => [POST]
  saveNote: async note => {
    const saveUrl = note._id
      ? `http://localhost:3001/notes/${note._id}`
      : "http://localhost:3001/notes";

    try {
      const response = await fetch(saveUrl, {
        method: note._id ? "PUT" : "POST",
        headers: {
          "Content-type": "application/json"
        },
        body: JSON.stringify(note)
      });
      return response.json();
    } catch (error) {
      return false;
    }

    /* return fetch(saveUrl, {
      method: note._id ? "PUT" : "POST",
      headers: {
        "Content-type": "application/json"
      },
      body: JSON.stringify(note)
    }).then(response => {
      return response.json();
    }); */
  },

  //UPDATE => [PUT]
  /*updateNote: (noteUpdated) => {
        return fetch(`http://localhost:3000/notes/${noteUpdated._id}`, {
            method: 'PUT',
            headers: {
                'content-type': 'application/json'
            },
            body: JSON.stringify(noteUpdated)
        })
            .then(response => {
                return response.json();
            });
    },*/

  //DELETE => [DELETE]
  deleteNoteById: noteId => {
    return fetch(`http://localhost:3001/notes/${noteId}`, {
      method: "DELETE"
    });
  }
};

export default NotesService;
