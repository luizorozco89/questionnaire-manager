export function toCapital(s) {
	var chatInUppercase = s[0];
	chatInUppercase = chatInUppercase.toUpperCase();
	var restOfString =  s.substr(1, s.length - 1);
	return `${chatInUppercase}${restOfString}`;
}

export function replaceInList(list, item, key) {
	const index = list.findIndex(_item => _item[key] === item[key]);
	let newList = list.slice(0, index);
	newList = newList.concat(item);
	newList = newList.concat(list.slice(index + 1));
	return newList;
}