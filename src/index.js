import React from 'react';
import ReactDOM from 'react-dom';
import registerServiceWorker from './registerServiceWorker';
import { Provider } from 'react-redux';
import { BrowserRouter } from 'react-router-dom';
import App from './common/components/app/app';
import configureStore from './common/store/configure-store';
import './index.css';
import '../node_modules/font-awesome/css/font-awesome.css'

const store = configureStore();

ReactDOM.render(
    <BrowserRouter>
        <Provider store={store} >
            <App />
        </Provider>
    </BrowserRouter>,
    document.querySelector('#root')
);

registerServiceWorker();
